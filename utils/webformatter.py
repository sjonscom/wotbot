from discord.ext.commands.formatter import HelpFormatter
from discord.ext.commands import Command
from discord.ext.commands import Group
import discord
import datetime
import inspect
import itertools
import pprint
import json


class WebFormatter(HelpFormatter):
    """
    Formats the help command. Adapted from discord.ext.commands.formatter.HelpFormatter
    """

    async def filter_command_list(self):
        return sorted(
            await super().filter_command_list(), key=lambda e: str.lower(e[0])
        )

    async def format(self, ctx, bot):
        """
        Formats the help page.
        """
        # Adapted from discord.ext.commands.formatter.HelpFormatter.format
        self.command = bot
        self.context = ctx
        self.total = {}

        def expand_group(group, path, name):
            """
            Recursively expands a Group into paginator
            :param group: the Group to expand
            :param prefix: prefix of the line
            """
            if not isinstance(group, Group):
                # path[prefix]=group.short_doc
                # path[group.name]=group.short_doc
                path[name] = [
                    group.short_doc.replace("'", "&#39;").replace('"', "&quot;"),
                    group.aliases,
                ]
            else:
                path[name] = [
                    {},
                    [
                        group.short_doc.replace("'", "&#39;").replace('"', "&quot;"),
                        group.aliases,
                    ],
                ]
                # print("xx",group.commands)
                # for subcommand in group.commands.copy().values():
                for subcommand in group.commands.copy():
                    # Build the prefix with group name so we get a nice list
                    # path[name][subcommand.name]
                    new_path = path[name][0]
                    expand_group(subcommand, new_path, subcommand.name)

        # Helper method for sorting by category (cog)
        def category(tup):
            # print(tup)
            cog = tup[1].cog_name
            # Unicode invisible space is there to set No Category last
            return cog if cog is not None else "Config"

        # data = self.command.commands.items()
        # if not self.is_cog() else self.context.bot.commands.items()
        data = sorted(await self.filter_command_list(), key=category)

        for category, commands in itertools.groupby(data, key=category):
            # self._paginator.new_page()
            # print(category)
            self.total[category] = {}
            # for c in commands:
            #    print(c)

            commands = list(commands)
            if len(commands) > 0:
                for name, command in commands:
                    # skip aliases
                    if name in command.aliases:
                        continue

                    if isinstance(command, Group):
                        # self.total[category][name]={}
                        path = self.total[category]
                        expand_group(command, path, name)
                        continue

                    self.total[category][name] = [
                        command.short_doc.replace("'", "&#39;").replace('"', "&quot;"),
                        command.aliases,
                    ]

        # pprint.pprint(self.total)
        # print(bot.description)
        return "var data=JSON.parse('{}');".format(json.dumps(self.total))
