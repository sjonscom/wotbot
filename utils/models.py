# import sqlite3 as lite
import aiosqlite as lite
import datetime
import humanize


class DB:
    def __init__(self, logger, bot):
        self.dbs = {
            "platoon": "data/platoon.db",
            "logger": "data/logger.db",
            "quiz": "data/quiz.db",
            "users": "data/users.db",
            "daily": "data/daily.db",
        }

        self.logger = logger
        self.bot = bot

    async def version(self):
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute("SELECT SQLITE_VERSION()") as cursor:
                data = await cursor.fetchone()
                return data

    async def get_platoons(self, limit=60):
        w = dict(limit=(datetime.datetime.utcnow() - datetime.timedelta(minutes=limit)))
        self.logger.debug(w)
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "SELECT id, user_name, tier, region,time_stamp FROM platoons where done=0 and time_stamp > $limit",
                w,
            ) as cursor:
                rows = await cursor.fetchall()
                dictlist = []
                for d in rows:
                    temp = []
                    for key in d.keys():
                        if key == "time_stamp":
                            temp.append(
                                humanize.naturaltime(
                                    datetime.datetime.utcnow() - d[key]
                                )
                            )
                        else:
                            temp.append(d[key])

                    dictlist.append(temp)
                return dictlist

    async def join_platoon(self, p_id):
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            await db.execute("update platoons set done=1 where id=?", (p_id,))
            await db.commit()

    async def check_platoon(self, user_id):
        w = dict(limit=(datetime.datetime.utcnow() - datetime.timedelta(minutes=20)))
        w["user_id"] = user_id
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "SELECT id, user_id FROM platoons where done=0 and time_stamp > $limit and user_id=$user_id",
                w,
            ) as cursor:
                res = await cursor.fetchall()
                self.logger.debug(len(res))
                if len(res):
                    return True
                else:
                    return False

    async def get_platoon(self, p_id):
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "select id, user_id from platoons where done=0 and id=?", (p_id,)
            ) as cursor:
                res = await cursor.fetchone()
                return res

    async def add_platoon(self, msg, tier, region):
        async with lite.connect(
            self.dbs["platoon"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute(
                "insert into platoons (user_name, user_id, tier, region, time_stamp,done) values(?,?,?,?,?,0)",
                (
                    msg.author.name,
                    msg.author.id,
                    tier,
                    region,
                    datetime.datetime.utcnow(),
                ),
            ) as cursor:
                await db.commit()
                return cursor.lastrowid
            return

    async def add_command(self, server, command):
        print(server, command)
        self.logger.debug((server, command))
        async with lite.connect(
            self.dbs["logger"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            await db.execute(
                "insert into commands(time_stamp, server, command) values(?,?,?)",
                (datetime.datetime.utcnow(), str(server), str(command)),
            )
            await db.commit()

    async def add_quiz(self, user, correct):
        self.logger.debug((user, correct))
        async with lite.connect(
            self.dbs["quiz"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            await db.execute(
                "insert or ignore into quiz(id, total, correct) values(?,0,0)",
                (str(user),),
            )
            await db.execute(
                "update quiz set total = total + 1, correct = correct + ? where id =?",
                (int(correct), str(user)),
            )
            await db.commit()

    async def get_quiz(self, user):
        self.logger.debug((user))
        async with lite.connect(
            self.dbs["quiz"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute(
                "select * from quiz where id=?", (str(user),)
            ) as cursor:
                res = await cursor.fetchone()
                return res

    async def get_quiz_top(self):
        async with lite.connect(
            self.dbs["quiz"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute(
                "select * from quiz order by correct desc limit 5"
            ) as cursor:
                res = await cursor.fetchall()
                return res

    async def popular_commands(self, limit):
        # self.logger.debug((server, command))
        async with lite.connect(
            self.dbs["logger"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute(
                "select command, count(id) as c from commands where command != 'command_doesn_exist' and command != 'help' group by command order by c desc limit ?;",
                (limit,),
            ) as cursor:
                res = await cursor.fetchall()
                return res

    async def popular_commands2(self, limit, days=5):
        w = dict(since=(datetime.datetime.utcnow() - datetime.timedelta(days=days)))
        w["limit"] = limit
        async with lite.connect(
            self.dbs["logger"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            async with db.execute(
                "select command, count(id) as c from commands where command != 'command_doesn_exist' and command != 'help' and time_stamp > $since group by command order by c desc limit $limit;",
                w,
            ) as cursor:
                res = await cursor.fetchall()
                return res

    async def add_user(self, user_id, nickname, region, token=None):

        self.logger.info(("saving user to db:", user_id, nickname, region, token))
        self.logger.debug((user_id))
        w = {"user_id": user_id}
        w["nickname"] = nickname
        w["region"] = region
        w["time_stamp"] = datetime.datetime.utcnow()
        w["token"] = token

        async with lite.connect(
            self.dbs["users"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            if token:

                await db.execute(
                    "insert or replace into users (account_id, nickname, region, time_stamp, token) values ($user_id, $nickname, $region,$time_stamp,$token)",
                    w,
                )
            else:
                await db.execute(
                    "insert or replace into users (account_id, nickname, region, time_stamp, token) values ($user_id, $nickname, $region,$time_stamp,(select token from users where account_id=$user_id))",
                    w,
                )
            await db.commit()

    async def get_user(self, user_id):
        self.logger.info(user_id)
        if not user_id:
            return None
        self.logger.info(user_id)
        w = {"user_id": int(user_id)}

        async with lite.connect(
            self.dbs["users"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:

            # db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "select account_id, region, token,nickname from users where account_id=$user_id",
                w,
            ) as cursor:
                rows = await cursor.fetchall()
                self.logger.info(("get user:", rows))
                if rows:
                    # return None
                    return rows[0]
                else:
                    return None

    async def get_daily_user(self, user):
        self.logger.debug((user))
        async with lite.connect(
            self.dbs["daily"],
            loop=self.bot.loop,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "select * from daily where account_id=?", (str(user),)
            ) as cursor:
                res = await cursor.fetchone()
                return res
