import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
from tabulate import tabulate
import datetime
import re
from flufl.lock import Lock  # locking
from pathlib import Path
import json


def is_manager(ctx):
    my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
    if my_file.is_file():
        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
        ) as json_data_file:
            data = json.load(json_data_file)
    else:
        data = {"managers": []}

    if data.get("managers", None) is None:
        print("no managers, reset managers")
        data["managers"] = []
    out = []
    print(data["managers"])
    if str(ctx.message.author.id) in data["managers"]:
        return True
    return False


# file locking
# https://flufllock.readthedocs.io/en/latest/


def is_server_admin(ctx):
    def predicate(ctx):
        bot.logger.debug(ctx.message.channel)
        return ctx.message.author.permissions_in(ctx.message.channel).administrator

    return commands.check(predicate)


class AceHeroes:
    """Top damage in ACEd battles"""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True, hidden=False)
    # @asyncio.coroutine
    async def aceheroes(self, ctx):
        """Server's table of highest ACEs in battles. Ace of Heroes"""
        self.bot.logger.info("ace heroes")

        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # self.bot.logger.debug("one")
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return

        if ctx.invoked_subcommand is None:
            self.bot.logger.info("ace heroes empty command")

            my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
            if my_file.is_file():
                with open(
                    "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
                ) as json_data_file:
                    data = json.load(json_data_file)
            else:
                await ctx.send(content="Nothing saved yet")
                return

            final_l = data.get("data", None)
            if final_l is None:
                await ctx.send(content=_("Nothing saved yet"))
                return

            embed = discord.Embed(
                title=_("Ace of Heroes"),
                description=_("Here they are!"),
                colour=234,
                type="rich",
                url=None,
            )
            final = sorted(final_l.items(), key=lambda x: int(x[0]))

            for k, v in final:
                entries = ["__player | damage | tank | screenshot__\n"]
                v.sort(key=lambda x: x["damage"], reverse=True)
                medals = [":first_place:", ":second_place:", ":third_place:"]
                tiers = [
                    ":one: ",
                    ":two:",
                    ":three:",
                    ":four:",
                    ":five:",
                    ":six:",
                    ":seven:",
                    ":eight:",
                    ":nine:",
                    ":keycap_ten:",
                ]
                l = 0
                # print(k)
                for i in v[:3]:
                    # print(i)
                    t = "{}**`{name}`** {damage} {tank} [:link:]({picture})".format(
                        medals[l], **i
                    )
                    l += 1
                    entries.append(t)
                try:
                    embed.add_field(
                        inline=False,
                        name="{}. tier".format(tiers[int(k) - 1]),
                        value="{}\n\uFEFF".format("\n".join(entries)),
                    )
                except:
                    print("error:", entries)

            # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
            # embed.set_image(url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/Screenshot_2017-01-01-00-24-51.png")
            # embed.set_footer(text="Designed for World of Tanks Blitz", icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png")
            # embed.set_image(url="https://cloud.githubusercontent.com/assets/3680926/21720861/c8d78a8e-d425-11e6-80a9-39ff85c5e30b.png")
            embed.set_footer(
                text="Best ACEs of this server. Can you beat them?",
                icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png",
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery4.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34910494-fa31f810-f8b6-11e7-8406-bd50e821bc15.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34910511-5f83169a-f8b7-11e7-835d-513a85d72b2d.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34918257-228164f6-f950-11e7-9dd0-745d630cffe8.png")
            embed.set_thumbnail(
                url="https://user-images.githubusercontent.com/3680926/34910530-92c76d8a-f8b7-11e7-9f06-99f0e56f3c18.png"
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def masters(self, ctx):
        """Ace of Heroes Masters of all servers"""
        self.bot.logger.info("ace heroes masters")
        # self.bot.logger.debug("one")

        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.invoked_subcommand is None:
            self.bot.logger.info("ace heroes empty command")

            # my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.server.id))
            p = Path("./stats/")
            # p=Path('../../run/wotbot/stats/') #testing only
            all_files = list(p.glob("*ace*.json"))
            complete_data = {}
            if all_files:
                for my_file in all_files:
                    if my_file.is_file():
                        with my_file.open() as json_data_file:
                            data = json.load(json_data_file)

                            record_data = data.get("data", None)
                            if record_data is not None:
                                if complete_data.get("data", None) is None:
                                    print("no data, reset data")
                                    complete_data["data"] = {}
                                for (
                                    record_tier,
                                    record_tier_data,
                                ) in record_data.items():
                                    # record_tier=str(record_all["tier"])
                                    # print("tier:", record_tier)
                                    # print("tier data", record_tier_data)

                                    if (
                                        complete_data["data"].get(record_tier, None)
                                        is None
                                    ):
                                        print("no tier, reset tier", record_tier)
                                        complete_data["data"][record_tier] = []
                                    for record_tier_data_item in record_tier_data:
                                        complete_data["data"][record_tier].append(
                                            record_tier_data_item
                                        )

            else:
                await ctx.send(content=_("Nothing saved yet"))
                return

            final_l = complete_data.get("data", None)
            # print(final_l)
            if final_l is None:
                await ctx.send(content=_("Nothing saved yet"))
                return

            embed = discord.Embed(
                title=_("Ace of Heroes Masters"),
                description=_("Here they are, ruling them all!"),
                colour=234,
                type="rich",
                url=None,
            )
            final = sorted(final_l.items(), key=lambda x: int(x[0]))

            for k, v in final:
                entries = ["__player | damage | tank | screenshot__\n"]
                v.sort(key=lambda x: x["damage"], reverse=True)
                medals = [":first_place:", ":second_place:", ":third_place:"]
                tiers = [
                    ":one: ",
                    ":two:",
                    ":three:",
                    ":four:",
                    ":five:",
                    ":six:",
                    ":seven:",
                    ":eight:",
                    ":nine:",
                    ":keycap_ten:",
                ]
                l = 0
                # print(k)
                for i in v[:3]:
                    # print(i)
                    t = "{}**`{name}`** {damage} {tank} [:link:]({picture})".format(
                        medals[l], **i
                    )
                    l += 1
                    entries.append(t)
                try:
                    embed.add_field(
                        inline=False,
                        name="{}. tier".format(tiers[int(k) - 1]),
                        value="{}\n\uFEFF".format("\n".join(entries)),
                    )
                except:
                    print("error in:", entries)

            # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
            # embed.set_image(url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/Screenshot_2017-01-01-00-24-51.png")
            # embed.set_footer(text="Designed for World of Tanks Blitz", icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png")
            # embed.set_image(url="https://cloud.githubusercontent.com/assets/3680926/21720861/c8d78a8e-d425-11e6-80a9-39ff85c5e30b.png")
            embed.set_footer(
                text="Best ACEs of all servers using AceHeroes stats in WotBot. Can you beat them?",
                icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png",
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery4.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34910494-fa31f810-f8b6-11e7-8406-bd50e821bc15.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34910511-5f83169a-f8b7-11e7-835d-513a85d72b2d.png")
            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34918257-228164f6-f950-11e7-9dd0-745d630cffe8.png")
            embed.set_thumbnail(
                url="https://user-images.githubusercontent.com/3680926/34910530-92c76d8a-f8b7-11e7-9f06-99f0e56f3c18.png"
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def view(self, ctx):
        """View list of aces together with IDs and who approved it"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content="Must run in a channel, not in Direct message")
            return
        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            await ctx.send(content="Nothing saved yet")
            return

        final_l = data.get("data", None)
        if final_l is None:
            await ctx.send(content=_("Nothing saved yet"))
            return

        embed = discord.Embed(
            title=_("Ace of Heroes"),
            description=_("Managers view:"),
            colour=234,
            type="rich",
            url=None,
        )
        final = sorted(final_l.items(), key=lambda x: int(x[0]))

        for k, v in final:
            entries = ["\uFEFF"]
            v.sort(key=lambda x: x["damage"], reverse=True)
            for i in v[:5]:
                t = "name: **`{name}`** damage: {damage} tank: {tank} id: **{id}**\nsubmitted: {sub} approved: {app}\n".format(
                    app=i["approved"]["name"], sub=i["submitter"]["name"], **i
                )
                # t="name: **{name}** damage: {damage} tank: {tank} id: **{id}**\nsubmitted: {sub} approved: {app}\n".format(app=ctx.message.server.get_member(i["approved"]["id"]).mention, sub=ctx.message.server.get_member(i["submitter"]["id"]).mention,**i)
                entries.append(t)

            embed.add_field(
                inline=False,
                name="Tier {}".format(k),
                value="{}".format("\n".join(entries)),
            )
            # print(entries)

        # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
        # embed.set_image(url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/Screenshot_2017-01-01-00-24-51.png")
        # embed.set_footer(text="Designed for World of Tanks Blitz", icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png")
        # embed.set_image(url="https://cloud.githubusercontent.com/assets/3680926/21720861/c8d78a8e-d425-11e6-80a9-39ff85c5e30b.png")
        embed.set_footer(
            text="Delete item via: ?aceheroes delete ID",
            icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png",
        )
        # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery4.png")
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/34918257-228164f6-f950-11e7-9dd0-745d630cffe8.png"
        )
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def cue(self, ctx):
        """View cue of submitted battles"""
        self.bot.logger.info("ace heroes cue")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"cue": []}

        final = data["cue"]

        embed = discord.Embed(
            title=_("Ace of Heroes"),
            description=_("Listing queue of submitted victories"),
            colour=234,
            type="rich",
            url=None,
        )
        # final=sorted(final_l.items(), key=lambda x: x[0])
        entries = ["__tier, name, damage, tank, picture, id, submitter__\n"]
        for k in final:
            # print(k)
            # v.sort(key=lambda x:x['damage'], reverse=True)
            t = "{tier} **`{name: <10}`**{damage: <10} {tank: <10}[:link:]({picture}) **{id}** {sub}".format(
                sub=k["submitter"]["name"], **k
            )
            entries.append(t)

        embed.add_field(
            inline=False, name="\uFEFF", value="{}".format("\n".join(entries))
        )

        # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
        # embed.set_image(url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/Screenshot_2017-01-01-00-24-51.png")
        embed.set_footer(
            text=_(
                "Queue managers, please process these, thank you! Delete via: ?aceheroes delete ID , Push item via ?aceheroes push ID , Push all via: ?aceheroes push all"
            ),
            icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png",
        )
        # embed.set_image(url="https://cloud.githubusercontent.com/assets/3680926/21720861/c8d78a8e-d425-11e6-80a9-39ff85c5e30b.png")
        # embed.set_thumbnail(url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/bt_logo_big.png")
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/34918257-228164f6-f950-11e7-9dd0-745d630cffe8.png"
        )
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def post(
        self,
        ctx,
        name: str = None,
        damage: str = None,
        tank: str = None,
        tier: str = None,
        picture: str = None,
    ):
        """Post: name damage tank tier picture. Spaces in quotes: 'Kuro Mori Mine' """
        self.bot.logger.info("ace heroes post to cue")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        error = ""
        if name is None:
            error += "name "
        if damage is None:
            error += "damage "
        else:
            try:
                damage = int(damage)
            except:
                error += _("damage must be a number, got {} ").format(damage)
        if tank is None:
            error += "tank "
        if tier is None:
            error += "tier "
        else:
            try:
                tier = int(tier)
                if tier not in range(1, 11):
                    error += _("tier must be a number between 0-10, got {}").format(
                        tier
                    )
            except:
                error += _("tier must be a number between 0-10, got {}").format(tier)

        if picture is None:
            error += "picture "
        if error != "":
            await ctx.send(_("Missing parameters: {}").format(error))
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"cue": [], "last_id": 0}

        if data.get("cue", None) is None:
            print("no cue, reset cue")
            data["cue"] = []

        cue_data = data.get("cue", None)

        if data.get("banned", None) is None:
            data["banned"] = []

        if str(ctx.message.author.id) in data["banned"]:
            lock.unlock()
            await ctx.send(_("User banned, sorry."))
            return

        if data.get("allowed", None) is None:
            data["allowed"] = []
        if len(data["allowed"]) > 0:
            if str(ctx.message.author.id) not in data["allowed"]:
                lock.unlock()
                await ctx.send(_("User not allowed to post, sorry."))
                return

        # if cue_unsorted is not None:
        #    cue_sorted = sorted(cue_unsorted, key=lambda x: x["id"], reverse=True)
        # else:
        #    cue_sorted=[]
        if cue_data:
            already_in = list(
                item
                for item in cue_data
                if item["submitter"]["id"] == str(ctx.message.author.id)
            )

            if len(already_in) > 9:
                lock.unlock()
                await ctx.send(
                    _(
                        "You have reached the limit of your entries in queue. Ask admin or moderator to process the queue"
                    )
                )
                return

        last_id = data.get("last_id", 0) + 1
        data["cue"].append(
            {
                "id": last_id,
                "damage": damage,
                "name": name,
                "tank": tank,
                "tier": tier,
                "picture": picture,
                "submitter": {
                    "id": str(ctx.message.author.id),
                    "name": ctx.message.author.name,
                },
            }
        )
        data["last_id"] = last_id

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

        await ctx.send(
            _(
                "Submitting all info i got: name: `{}`, damage: {}, tank: {}, tier: {} and picture, with id: {}"
            ).format(name, damage, tank, tier, last_id)
        )

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def push(self, ctx, id: str = None):
        """Push from cue to list"""
        self.bot.logger.info("aceheroes push")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if id is None:
            await ctx.send(content=_("Must supply id"))
            return

        if id != "all":
            try:
                id = int(id)
            except:
                await ctx.send(content=_("ID must be a number"))
                return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            lock.unlock()
            await ctx.send(content=_("Nothing saved yet"))
            return

        if data.get("data", None) is None:
            print("no data, reset data")
            data["data"] = {}
        cue_unsorted = data.get("cue", None)
        if cue_unsorted is None:
            lock.unlock()
            await ctx.send(content=_("Nothing saved yet"))
            return
        if id == "all":
            print("process all")
            for record_all in (item for item in data["cue"]):
                record_all["approved"] = {
                    "id": str(ctx.message.author.id),
                    "name": ctx.message.author.name,
                }
                # record_id=record_all["id"]
                record_tier = str(record_all["tier"])
                # print("tier:", record_tier)

                if data["data"].get(record_tier, None) is None:
                    print("no tier, reset tier", record_tier)
                    # print(data["data"])
                    data["data"][record_tier] = []
                # print("push:", record_all)
                data["data"][record_tier].append(record_all)
            data["cue"] = []
        else:
            record = next((item for item in cue_unsorted if item["id"] == id), None)
            if record is None:
                lock.unlock()
                await ctx.send(content=_("ID doesn't exist"))
                return
            else:
                index = cue_unsorted.index(record)
                record["approved"] = {
                    "id": str(ctx.message.author.id),
                    "name": ctx.message.author.name,
                }
                tier = str(record["tier"])
                if data["data"].get(tier, None) is None:
                    print("no tier, reset tier")
                    data["data"][tier] = []
                data["data"][tier].append(record)
                data["cue"].pop(index)

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        await ctx.send(content=_("Done"))
        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def delete(self, ctx, id: str = None):
        """Delete by ID"""
        self.bot.logger.info("aceheroes delete")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if id is None:
            await ctx.send(content=_("Must supply id"))
            return
        try:
            id = int(id)
        except:
            await ctx.send(content=_("ID must be a number"))
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            lock.unlock()
            await ctx.send(content=_("Nothing saved yet"))
            return
        record = None
        cue_unsorted = data.get("cue", None)
        record = next((item for item in cue_unsorted if item["id"] == id), None)
        # print("cue record:", record)
        if record is not None:
            # print("erasing from cue:", record)
            index = cue_unsorted.index(record)
            data["cue"].pop(index)

        index_r = None
        data_unsorted = data.get("data", None)
        for tier, items in data_unsorted.items():
            # print(tier)
            for u in items:
                # print(u)
                if u["id"] == id:
                    tier_r = tier
                    index_r = items.index(u)

        if index_r is not None:
            # print("erasing from data:", tier_r, index_r, u)
            data["data"][tier_r].pop(index_r)

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        await ctx.send(content=_("Done"))
        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def manager(self, ctx):
        """List of managers"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        self.bot.logger.info("ace heroes managers")
        # self.bot.logger.debug("one")
        self.bot.logger.info("ace heroes managers empty")
        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"managers": []}

        if data.get("managers", None) is None:
            print("no managers, reset managers")
            data["managers"] = []
        out = []
        print(data["managers"])
        for user in data["managers"]:
            if ctx.message.guild.get_member(int(user)) is not None:
                out.append(ctx.message.guild.get_member(int(user)).name)

        await ctx.send("Managers: {}".format(", ".join(out)))

        return True

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def manageradd(self, ctx):
        """Add managers via list of mentions"""
        self.bot.logger.info("aceheroes managers add")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        # print(ctx.message.mentions)
        if not ctx.message.author.permissions_in(ctx.message.channel).administrator:
            await ctx.send(content=_("Must be channel admin"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_(
                    "Users to be made managers must be mentioned in this message."
                )
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"managers": []}

        if data.get("managers", None) is None:
            # print("no managers, reset managers")
            data["managers"] = []

        for user in ctx.message.mentions:
            data["managers"].append(str(user.id))

        data["managers"] = list(set(data["managers"]))
        print("xxx")
        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def managerremove(self, ctx):
        """Remove managers via list of mentions"""
        print(ctx.message.mentions)
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        self.bot.logger.info("aceheroes managers remove")
        if not ctx.message.author.permissions_in(ctx.message.channel).administrator:
            await ctx.send(content=_("Must be channel admin"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_(
                    "Users to be removed from managers must be mentioned in this message."
                )
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"managers": []}

        if data.get("managers", None) is None:
            print("no managers, reset managers")
            data["managers"] = []
        print(data["managers"])
        for user in ctx.message.mentions:
            if str(user.id) in data["managers"]:
                data["managers"].remove(str(user.id))

        # data["managers"]=list(set(data["managers"]))

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def banned(self, ctx):
        """See banned list"""
        self.bot.logger.info("ace heroes banned")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        # self.bot.logger.debug("one")
        self.bot.logger.info("ace heroes banned")
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"banned": []}

        if data.get("banned", None) is None:
            print("no banned, reset banned")
            data["banned"] = []
        out = []
        for user in data["banned"]:
            if ctx.message.guild.get_member(int(user)) is not None:
                out.append(ctx.message.guild.get_member(int(user)).name)

        await ctx.send("Banned: {}".format(", ".join(out)))

        return True

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def bannedadd(self, ctx):
        """Ban via list of mentions"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        self.bot.logger.info("aceheroes banned add")
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_("Users to be banned must be mentioned in this message.")
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"banned": []}

        if data.get("banned", None) is None:
            print("no banned, reset banned")
            data["banned"] = []

        for user in ctx.message.mentions:
            data["banned"].append(str(user.id))

        data["banned"] = list(set(data["banned"]))

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def bannedremove(self, ctx):
        """Unban via list of mentions"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        self.bot.logger.info("aceheroes banned remove")
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_(
                    "Users to be removed from banned must be mentioned in this message."
                )
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"banned": []}

        if data.get("banned", None) is None:
            print("no banned, reset banned")
            data["banned"] = []

        for user in ctx.message.mentions:
            if str(user.id) in data["banned"]:
                data["banned"].remove(str(user.id))

        data["banned"] = list(set(data["banned"]))

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def allowonly(self, ctx):
        """See allowonly list"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        self.bot.logger.info("ace heroes allow only")
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        # self.bot.logger.debug("one")
        self.bot.logger.info("ace heroes allow only")
        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"allowed": []}

        if data.get("allowed", None) is None:
            print("no allowed, reset allowed")
            data["allowed"] = []
        out = []
        for user in data["allowed"]:
            if ctx.message.guild.get_member(int(user)) is not None:
                out.append(ctx.message.guild.get_member(int(user)).name)

        await ctx.send("Allowed to post: {}".format(", ".join(out)))

        return True

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def allowonlyadd(self, ctx):
        """Add to allowed via list of mentions"""
        self.bot.logger.info("aceheroes allowed add")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_("Users to be allowed must be mentioned in this message.")
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"allowed": []}

        if data.get("allowed", None) is None:
            print("no allowed, reset allowed")
            data["allowed"] = []

        for user in ctx.message.mentions:
            data["allowed"].append(str(user.id))

        data["allowed"] = list(set(data["allowed"]))

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()

    @aceheroes.command(pass_context=True)
    # @asyncio.coroutine
    async def allowonlyremove(self, ctx):
        """Remove from allowed via list of mentions"""
        self.bot.logger.info("aceheroes allowed remove")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.message.guild is None:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))
            return
        if not (
            ctx.message.author.permissions_in(ctx.message.channel).administrator
            or is_manager(ctx)
        ):
            await ctx.send(content=_("Must be channel admin or manager"))
            return True

        if len(ctx.message.mentions) < 1:
            await ctx.send(
                content=_(
                    "Users to be removed from allowed must be mentioned in this message."
                )
            )
            return

        lock = Lock("./stats/{}-aceheroes.json.lock".format(ctx.message.guild.id))
        lock.lock()

        my_file = Path("./stats/{}-aceheroes.json".format(ctx.message.guild.id))
        if my_file.is_file():
            with open(
                "./stats/{}-aceheroes.json".format(ctx.message.guild.id)
            ) as json_data_file:
                data = json.load(json_data_file)
        else:
            data = {"allowed": []}

        if data.get("allowed", None) is None:
            print("no allowed, reset allowed")
            data["allowed"] = []

        for user in ctx.message.mentions:
            if str(user.id) in data["allowed"]:
                data["allowed"].remove(str(user.id))

        data["allowed"] = list(set(data["allowed"]))

        with open(
            "./stats/{}-aceheroes.json".format(ctx.message.guild.id), "w"
        ) as json_data_file:
            json.dump(data, json_data_file)

        lock.unlock()


def setup(bot):
    bot.add_cog(AceHeroes(bot))
