import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import datetime
import math
import humanize
import time


class ClanStats:
    """Clan statistics"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=["calnactivity"])
    # @asyncio.coroutine
    async def clanactivity(self, ctx, *, clan_name: str = None):
        """Clan activity summary. Clan name or empty for current user's clan"""
        self.bot.logger.info(("clan activity:", clan_name))
        color_list = "javascript"
        tstart = time.time()
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        clan_id = None
        if clan_name is None:
            player, region, token, ownuser = await self.bot.wg.get_local_player(
                clan_name, ctx
            )

            if not player:
                return
            self.bot.logger.info(("clan activity:", clan_name))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan[player_id] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                if clan[player_id]["clan_id"] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id]["clan_id"])
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, clan_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send(_("Clan not found"))

        if clan_id is not None:

            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id)
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}".format(clan_id),
            )
            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            clan_members_count = clan_info[clan_id]["members_count"]
            clan_members_list_helper = clan_info[clan_id]["members_ids"]
            clan_members_list = []

            embed = discord.Embed(
                title=clan_tag, description=clan_name, colour=234, type="rich"
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

            for i in clan_members_list_helper:
                clan_members_list.append(
                    [i, datetime.datetime.fromtimestamp(0), 0, 0, 0, 0]
                )
            out = "```md\n{clan}@{region}, {count} {members}:\n\n{player: <13} {afk:>0} {battles: >5} {wr: >4} {damage: >7}\n```".format(
                clan=clan_tag,
                region=region,
                count=clan_members_count,
                members=_("members"),
                player=_("playername"),
                afk=_("days afk"),
                battles=_("battles"),
                wr=_("wr"),
                damage="damage",
            )
            msg = await ctx.send(out)
            # await ctx.trigger_typing()

            clan_players = ",".join(str(x) for x in clan_members_list_helper)
            # def a():
            # return self.bot.wg.get_players_by_ids(clan_players,region)
            clan_players_returned, region = await self.bot.wg.get_players_by_ids_a(
                clan_players, region
            )
            # future=self.bot.loop.run_in_executor(None,a)
            # clan_players_returned, region =await future
            for clan_member, member_info in clan_players_returned.items():
                now = datetime.datetime.now()
                if member_info is not None:
                    for x, value in enumerate(clan_members_list):
                        # print(type(value[0]), type(clan_member))
                        if value[0] == int(clan_member):
                            try:
                                clan_members_list[x] = [
                                    clan_member,
                                    datetime.datetime.fromtimestamp(
                                        member_info["last_battle_time"]
                                    ),
                                    member_info["nickname"],
                                    (
                                        now
                                        - datetime.datetime.fromtimestamp(
                                            member_info["last_battle_time"]
                                        )
                                    ).days,
                                    member_info["statistics"]["all"]["battles"],
                                    (
                                        member_info["statistics"]["all"]["wins"]
                                        / member_info["statistics"]["all"]["battles"]
                                    )
                                    * 100,
                                    member_info["statistics"]["all"]["damage_dealt"]
                                    / member_info["statistics"]["all"]["battles"],
                                ]
                            except:
                                # self.bot.logger.debug(member_info[clan_member])
                                clan_members_list[x] = [
                                    clan_member,
                                    datetime.datetime.fromtimestamp(
                                        member_info["last_battle_time"]
                                    ),
                                    member_info["nickname"],
                                    (
                                        now
                                        - datetime.datetime.fromtimestamp(
                                            member_info["last_battle_time"]
                                        )
                                    ).days,
                                    member_info["statistics"]["all"]["battles"],
                                    member_info["statistics"]["all"]["wins"],
                                    member_info["statistics"]["all"]["damage_dealt"],
                                ]
                # clan_members_list=sorted(clan_members_list, key=itemgetter(3,5),reverse=True)
            clan_members_list = sorted(clan_members_list, key=lambda x: (x[3], -x[5]))
            out_list = "```{color}\n".format(color=color_list)
            for i in clan_members_list:
                if i[2]:
                    out_list += "{: <16} {: >5} {: >6} {:>5.2f} {:>7.2f}\n".format(
                        i[2][:16][:15], i[3], i[4], i[5], i[6]
                    )
            out_list += "```"
            out_total = out_list
            self.bot.logger.debug(len(out_total))
            if len(out_total) > 2000:
                # just no time and energy to do better now
                out_list = "```{color}\n".format(color=color_list)
                for i in clan_members_list[: math.ceil(len(clan_members_list) / 2.0)]:
                    if i[2]:
                        out_list += "{: <16} {: >5} {: >6} {:>5.2f} {:>7.2f}\n".format(
                            i[2][:16][:15], i[3], i[4], i[5], i[6]
                        )
                out_list += "```"
                await ctx.send(out_list)
                out_list = "```{color}\n".format(color=color_list)
                for i in clan_members_list[
                    len(clan_members_list) - math.floor(len(clan_members_list) / 2.0) :
                ]:
                    if i[2]:
                        out_list += "{: <16} {: >5} {: >6} {:>5.2f} {:>7.2f}\n".format(
                            i[2][:16][:15], i[3], i[4], i[5], i[6]
                        )
                out_list += "```"
                await ctx.send(out_list)
            else:
                self.bot.logger.debug(len(out_total))
                await ctx.send(out_total)
            # await ctx.send(out_total)
            # self.bot.logger.debug(clan_members_list)
            print(humanize.naturaldelta(tstart - time.time()))
            self.bot.logger.info("end of clan stats task")


def setup(bot):
    bot.add_cog(ClanStats(bot))
