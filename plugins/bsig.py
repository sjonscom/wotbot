import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random


import cv2
import math
import numpy as np
from PIL import Image, ImageDraw
from random import randint
import aiohttp
import tempfile

region_wg_to_bs = {"na": "com", "eu": "eu", "ru": "ru", "asia": "asia"}


class BlitzStarsStats:
    """Staistics and signatures from BlitzStars"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=["blitzsig2", "bsigmeme"])
    # @asyncio.coroutine
    async def bsig2(self, ctx, *, player_name: str = None):
        """BlitzStars memeified :smiley:"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("bsig:", player_name))

        # self.bot.logger.debug((player, region))
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            _hash = random.getrandbits(128)
            # self.bot.logger.debug(_hash)
            overlays = ["./hcr2.png", "./bs.png"]
            overlay = random.choice(overlays)
            xcal = 284
            ycal = 33
            # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
            clan = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="accountinfo",
                parameter="account_id={}".format(player_id),
            )
            # player_tanks=player_tanks[player_id]

            if clan[player_id] is not None:
                clan_id = str(clan[player_id]["clan_id"])
                if clan_id == "23762":  # argo
                    overlay = "./argo.png"
            overlay = "./hcr2.png"
            # self.bot.logger.debug("https://www.blitzstars.com/api/sigs/eu/{}.png".format(player_id))
            # image=await io.BytesIO(r.content)

            title_text = _("BlitzStars statistics")
            embed = discord.Embed(
                title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                    title=title_text,
                    for_=_("for"),
                    nick=player_nick,
                    region=region,
                    confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                    contributor=await self.bot.dc.contributor(ctx, player, region),
                ),
                description="",
                colour=234,
                type="rich",
                url=await self.bot.dc.confirmed_url(ctx, ownuser),
                # url="https://www.blitzstars.com/player/{}/{}".format(
                #    region_wg_to_bs[region], player_nick
                # ),
            )
            # embed.set_author(name="BlitzStars", url="https://www.blitzstars.com/", icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png")
            embed.set_thumbnail(
                url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png"
            )
            embed.set_footer(
                text=_("Signature by BlitzStars, memeified by {}").format(
                    "\U0001f451 WotBot"
                ),
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )

            url = "https://www.blitzstars.com/api/sigs/{}/{}.png?{}".format(
                region_wg_to_bs[region], player_id, _hash
            )
            async with aiohttp.ClientSession(loop=self.bot.loop) as client:
                async with client.get(url) as r:
                    data = None
                    if r.status == 200:
                        data = await r.read()
                    await r.release()
                    if data is not None:
                        with tempfile.NamedTemporaryFile(
                            suffix=".png", delete=True
                        ) as f:
                            f.write(data)

                            # print(f.name)
                            par0 = 2
                            par1 = 30
                            par2 = 5
                            car = Image.open(overlay)
                            # size = 45,30
                            img = cv2.imread(f.name)
                            destination = Image.open(f.name)
                            sub_image = img[33:85, 284:437]
                            gray = cv2.cvtColor(sub_image, cv2.COLOR_BGR2GRAY)
                            edges = cv2.Canny(gray, 80, 12)
                            lines = cv2.HoughLinesP(
                                edges, 1, math.pi / 180, par0, None, par1, par2
                            )
                            if lines is not None:
                                for x1, y1, x2, y2 in lines[randint(0, len(lines) - 1)]:
                                    # for x1,y1,x2,y2 in lines[1]:
                                    angle = (
                                        np.rad2deg(np.arctan2(y2 - y1, x2 - x1)) * -1
                                    )
                                    draw = ImageDraw.Draw(destination)
                                    # draw.line((x1+xcal,y1+ycal, x2+xcal, y2+ycal), fill=128, width=3)
                                    # draw.point((x1+xcal, y1+ycal), fill='blue')
                                    # destination=Image.new(destination)
                                    print("angle:", angle)
                                    # print(x1,x2,y1,y2)
                                    # break
                            else:
                                x1 = randint(0, 100)
                                y1 = -4
                                # angle=randint(0,180)
                                angle = 0
                            # rot = car.resize(size, Image.ANTIALIAS).rotate(-angle, expand=1 )            #destination.paste(img,(x1,y1))
                            old_width, old_height = car.size
                            rot = car.rotate(
                                angle, expand=1
                            )  # destination.paste(img,(x1,y1))
                            # rot=car
                            width, height = rot.size
                            fin = rot.convert("RGBA")
                            left = int(math.sin(angle) * old_height)

                            # fin=car.convert('RGBA')
                            # destination.paste(fin,(x1+xcal,y1+ycal),fin)
                            # print(x1,x2,y1,y2)
                            # destination.paste(fin,(x1+xcal,y1+ycal2),fin)
                            xx, yy = x1 + xcal - left, y1 + ycal - height
                            # xx,yy=x1+xcal,y1+ycal-height
                            # xx,yy=x1+xcal,y1+ycal
                            if yy < 0:
                                yy = 0
                            # print(xx,yy)
                            # destination.paste(fin,(xx,yy),fin)
                            destination.paste(fin, (xx, yy), fin)
                            # destination.paste(fin,(xx,yy))
                            destination.save(f.name, format="png")
                            # t=await self.bot.upload(f.name, destination=self.bot.user.id,content="my msg", filename="blitzstars.png")
                            t = await self.bot.get_channel(414445407729352704).send(
                                file=discord.File(f.name, "blitzstars.png")
                            )
                            img_url = t.attachments[0].url
                            embed.set_image(url=img_url)
                    else:
                        embed.add_field(
                            name=_("No data"), value=_("No data on BlitzStars")
                        )

                    try:
                        await ctx.send(content=None, embed=embed)
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.send(
                            content=_(
                                "Please enable Embed links permission for wotbot."
                            )
                        )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)

    @commands.command(pass_context=True, aliases=["blitzsig"])
    # @asyncio.coroutine
    async def bsig(self, ctx, *, player_name: str = None):
        """BlitzStars WR signature."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)
        print(ctx.message.content)

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("bsig:", player_name))

        # self.bot.logger.debug((player, region))
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            _hash = random.getrandbits(128)
            # self.bot.logger.debug(_hash)
            # self.bot.logger.debug("https://www.blitzstars.com/api/sigs/eu/{}.png".format(player_id))
            img_url = "https://www.blitzstars.com/api/sigs/{}/{}.png?{}".format(
                region_wg_to_bs[region], player_id, _hash
            )
            self.bot.logger.debug(img_url)
            title_text = _("BlitzStars statistics")
            embed = discord.Embed(
                title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                    title=title_text,
                    for_=_("for"),
                    nick=player_nick,
                    region=region,
                    confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                    contributor=await self.bot.dc.contributor(ctx, player, region),
                ),
                description="",
                colour=234,
                type="rich",
                url=await self.bot.dc.confirmed_url(ctx, ownuser),
                # url="https://www.blitzstars.com/player/{}/{}".format(
                #    region_wg_to_bs[region], player_nick
                # ),
            )
            embed.set_image(url=img_url)
            embed.set_author(
                name="BlitzStars",
                url="https://www.blitzstars.com/",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            embed.set_thumbnail(
                url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png"
            )
            embed.set_footer(
                text="Signature by BlitzStars",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(BlitzStarsStats(bot))
