import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math
import itertools

percents = {
    0.0: "https://wotblitz.com/newstatic/images/twister_icon.png",
    1.0: "https://user-images.githubusercontent.com/3680926/34320161-ae8db030-e7f3-11e7-8e09-442ce9c41bde.png",
    2.0: "https://user-images.githubusercontent.com/3680926/34320162-aeb1d028-e7f3-11e7-8ff5-54e1646991a6.png",
    3.0: "https://user-images.githubusercontent.com/3680926/34320163-aeda1452-e7f3-11e7-9bc7-eae519371a5f.png",
    4.0: "https://user-images.githubusercontent.com/3680926/34320165-aef8bca4-e7f3-11e7-8f0e-c25f9554c7e8.png",
    5.0: "https://user-images.githubusercontent.com/3680926/34320166-af1bad9a-e7f3-11e7-9dc3-317b4069ddf3.png",
    6.0: "https://user-images.githubusercontent.com/3680926/34320167-af4208a0-e7f3-11e7-94f0-6f88c058596e.png",
    7.0: "https://user-images.githubusercontent.com/3680926/34320168-af623576-e7f3-11e7-93b5-b21ce29752b7.png",
    8.0: "https://user-images.githubusercontent.com/3680926/34320169-af805056-e7f3-11e7-9872-31ecfa802229.png",
    9.0: "https://user-images.githubusercontent.com/3680926/34320170-af9e6492-e7f3-11e7-944c-b1d480623d6e.png",
    10.0: "https://user-images.githubusercontent.com/3680926/34318931-d9e3abce-e7d3-11e7-9f27-ae725c0cf109.png",
}

# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True, hidden=False, aliases=["thanks", "tnx", "tanx"]
    )
    # @asyncio.coroutine
    async def tanks(self, ctx, *, player_name: str = None):
        """Show best tanks of a player."""
        autorun = False
        color_list = "javascript"
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=dict(self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier"))
        # all_vehicles['64081']= {'tier': 1, 'name': 'mk1 heavy'}
        # all_vehicles['56609']= {'tier': 7, 'name': 'T49A'}
        # all_vehicles['12545']= {'tier': 9, 'name': 'K-91'}
        all_vehicles = await self.bot.wg.get_all_vehicles()

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("readstats:", player_name))

        # self.bot.logger.debug(len(player))
        # self.bot.logger.debug(player)
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tiers = {}
            # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id, all.wins, all.battles, all.damage_dealt,all.xp".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]
            if player_tanks is None:
                await ctx.send(_("No data for `{}@{}` ").format(player_nick, region))
                return

            class Tanks(object):
                def __init__(self):
                    self.all = {}

                def add(self, tank, data):
                    t = 0
                    if str(tank["tank_id"]) in all_vehicles:
                        tier = int(all_vehicles[str(tank["tank_id"])]["tier"])
                    else:
                        print("not in tanko", tank["tank_id"])
                    if tier < 1:
                        t = "zero"
                    elif tier < 5:
                        t = "low"
                    else:
                        t = "high"
                    if t not in self.all:
                        self.all[t] = {}
                    self.all[t][tank["tank_id"]] = data

                def get(self, tier, index):
                    if tier not in self.all:
                        return None
                    max_ = sorted(
                        self.all[tier].items(), key=lambda k: k[1][index], reverse=True
                    )
                    top_ = dict(max_[:5])
                    res_ = []

                    for t, v in top_.items():
                        if str(t) in all_vehicles:
                            res_.append([all_vehicles[str(t)]["name"], v[index]])
                    return res_

            tanks = Tanks()
            total_btls = 0
            for tank in player_tanks:
                total_btls += tank["all"]["battles"]

            if (total_btls * 0.01) > 100:
                total_btls = 99
            else:
                total_btls = total_btls * 0.01

            for tank in player_tanks:

                allt = tank["all"]
                if int(allt["battles"]) > total_btls:
                    tanks.add(
                        tank,
                        [
                            allt["battles"],
                            allt["wins"] / allt["battles"] * 100,
                            allt["damage_dealt"] / allt["battles"],
                            allt["xp"] / allt["battles"],
                        ],
                    )
            # print(tanks.all)

            title_text = _("Best tanks of player")
            embed = discord.Embed(
                title="{title} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                    title=title_text,
                    for_=_("for"),
                    nick=player_nick,
                    region=region,
                    confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                    contributor=await self.bot.dc.contributor(ctx, player, region),
                ),
                colour=234,
                type="rich",
                url=await self.bot.dc.confirmed_url(ctx, ownuser),
                # url="https://www.blitzstars.com/player/{}/{}".format(
                #    region, player_nick
                # ),
            )
            tiers = ["zero", "low", "high"]
            tier_label = {
                "zero": _("Tanks not in tankopedia"),
                "low": _("Tiers 1 - 4"),
                "high": _("Tiers 5 - 10"),
            }
            for t in tiers:
                if not tanks.get(t, 0):
                    continue
                embed.add_field(
                    name=tier_label[t],
                    value=_("Listing only tanks with more then {} battles.").format(
                        int(total_btls)
                    ),
                    inline=False,
                )
                embed.add_field(
                    name=_("Number of battles"),
                    value="```{}\n{}```".format(color_list, tabulate(tanks.get(t, 0))),
                    inline=False,
                )
                embed.add_field(
                    name=_("Winrate"),
                    value="```{}\n{}```".format(color_list, tabulate(tanks.get(t, 1))),
                    inline=False,
                )
                embed.add_field(
                    name=_("Damage per battle"),
                    value="```{}\n{}```".format(color_list, tabulate(tanks.get(t, 2))),
                    inline=False,
                )
                embed.add_field(
                    name=_("Experience per battle"),
                    value="```{}\n{}```".format(color_list, tabulate(tanks.get(t, 3))),
                    inline=False,
                )
                embed.add_field(name="\uFEFF", value="\uFEFF")
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            embed.set_footer(text=_("Stats by \U0001f451 WotBot."))
            # embed.set_thumbnail(url=percents[round(wr_ttl, -1) / 10])
            try:
                msg = await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
