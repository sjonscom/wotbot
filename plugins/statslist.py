import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import pickle
from pathlib import Path
import json
import os
from tabulate import tabulate


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    # @asyncio.coroutine
    async def statslist(self, ctx):
        """Maintain your list of player names for stats."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if ctx.invoked_subcommand is None:
            my_file = Path("./stats/{}-list.pickle".format(ctx.message.author.id))
            if my_file.is_file():
                with open(
                    "./stats/{}-list.pickle".format(ctx.message.author.id), "rb"
                ) as storage:
                    my_list = pickle.load(storage)
                    if my_list is not None:
                        players = my_list["default"]
                        names = []

                    if players:
                        for player_id in players:
                            player, region = await self.bot.wg.get_player_by_id_a(
                                player_id
                            )
                            if player:
                                player = player[player_id]
                                if player:
                                    player_name = player.get("nickname", "no name")
                                    names.append(["{}@{}".format(player_name, region)])
                    # print(len(names))
                    output = tabulate(names, tablefmt="grid")
                    # print(output)
                    embed = discord.Embed(
                        title=_("Stats saved for:"),
                        description="```{}```".format(output),
                        colour=234,
                        type="rich",
                    )
                    # embed.set_footer(text="{}".format(foot))
                    try:
                        msg = await ctx.send(content=None, embed=embed)
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.send(
                            content=_(
                                "Please enable Embed links permission for wotbot."
                            )
                        )

            else:
                await ctx.send("nothing saved, use ?statslist save name1 name2 name3")

    @statslist.command(pass_context=True)
    # @asyncio.coroutine
    async def save(self, ctx, *, players_names: str = None):
        """Overwrites current list with new names"""

        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        no_players = []
        yes_players = []
        save_players = []

        if players_names is None:
            # must be something
            # print error
            await ctx.send(
                _("edit requires list of names: ?statslist edit name1 name2 name3")
            )
            return
        else:
            players_names = players_names.split()

        self.bot.logger.info(("statslist save:", players_names))

        await self.bot.dc.typing(ctx)
        for player_name in players_names:
            player, region = await self.bot.wg.get_player_a(player_name, ctx.message)

            print("found", player)
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                if player_id not in save_players:
                    save_players.append(player_id)
                    yes_players.append(player_name)
            else:
                no_players.append(player_name)

        with open(
            "./stats/{}-list.pickle".format(ctx.message.author.id), "wb"
        ) as storage:
            pickle.dump({"default": save_players}, storage)
        # await ctx.send("Saving list of names: {",".join(yes_players)}\n{"Not saving (username not found):" if no_players else ""} {",".join(no_players)}")
        await ctx.send(
            "{} `{}`\n{} `{}`".format(
                _("Saving list of names:"),
                ",".join(yes_players),
                _("Not saving (username not found):") if no_players else "",
                ",".join(no_players),
            )
        )

    @statslist.command(pass_context=True)
    # @asyncio.coroutine
    async def clear(self, ctx):
        """Removes list completely"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        my_file = Path("./stats/{}-list.pickle".format(ctx.message.author.id))
        if my_file.is_file():
            os.remove("./stats/{}-list.pickle".format(ctx.message.author.id))
            await ctx.send("List removed")
        else:
            await ctx.send(_("nothing saved, use ?statslist save name1 name2 name3"))

    @statslist.command(pass_context=True)
    # @asyncio.coroutine
    async def add(self, ctx, *, players_names: str = None):
        """Adds names to existing list"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext

        no_players = []
        yes_players = []
        save_players = []
        existed_players = []

        my_file = Path("./stats/{}-list.pickle".format(ctx.message.author.id))
        if my_file.is_file():
            with open(
                "./stats/{}-list.pickle".format(ctx.message.author.id), "rb"
            ) as storage:
                my_list = pickle.load(storage)
                if my_list is not None:
                    save_players = my_list["default"]

        if players_names is None:
            # must be something
            # print error
            await ctx.send(
                _("add requires list of names: ?statslist add name1 name2 name3")
            )
            return
        else:
            players_names = players_names.split()

        self.bot.logger.info(("statlist add:", players_names))

        await self.bot.dc.typing(ctx)
        for player_name in players_names:
            player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
            print("found", player)

            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                if player_id not in save_players:
                    save_players.append(player_id)
                    yes_players.append(player_name)
                else:
                    existed_players.append(player_name)
            else:
                no_players.append(player_name)

        if save_players:
            with open(
                "./stats/{}-list.pickle".format(ctx.message.author.id), "wb"
            ) as storage:
                pickle.dump({"default": save_players}, storage)
        await ctx.send(
            " `{}`\n{} `{}`\n{} `{}`".format(
                _("Saving list of names:"),
                ",".join(yes_players),
                _("Not saving (username not found):") if no_players else "",
                ",".join(no_players),
                _("Not saving (already existed):") if existed_players else "",
                ",".join(existed_players),
            )
        )


def setup(bot):
    bot.add_cog(UserStats(bot))
