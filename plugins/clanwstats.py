import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import datetime
import pickle
import humanize
import time


class ClanStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        hidden=True,
        aliases=["saveclanstats", "savecalnstats", "calnsavestats"],
    )
    # @asyncio.coroutine
    async def clansavestats(self, ctx, *, clan_name: str = None):
        """Save checkpoint for clan stats. Clan name or empty for current user's clan"""
        self.bot.logger.info(("clan activity:", clan_name))
        tstart = time.time()
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)
        clan_id = None
        if clan_name is None:
            player, region, token, ownuser = await self.bot.wg.get_local_player(
                clan_name, ctx
            )

            if not player:
                return
            self.bot.logger.info(("clan activity:", clan_name))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan[player_id] is None:
                    await ctx.send(
                        "Player `{}@{}` has no active clan.".format(player_nick, region)
                    )
                    return
                if clan[player_id]["clan_id"] is None:
                    await ctx.send(
                        "Player `{}@{}` has no active clan.".format(player_nick, region)
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id]["clan_id"])
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, clan_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send("clan not found")

        if clan_id is not None:
            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id)
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}".format(clan_id),
            )
            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            clan_members_count = clan_info[clan_id]["members_count"]
            clan_members_list_helper = clan_info[clan_id]["members_ids"]
            save_members_list = []
            members_names = {}

            embed = discord.Embed(
                title=clan_tag, description=clan_name, colour=234, type="rich"
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            embed.add_field(
                name="Saving data for `{}@{}`".format(clan_tag, region),
                value="{} members.".format(clan_members_count),
            )
            embed.add_field(
                name="\uFEFF",
                value="See differential clan stats at anytime with ?clanstats",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content="Please enable Embed links permission for wotbot."
                )

            # for i in clan_members_list_helper:
            # id, battles count,wins,rating
            # save_members_list.append([i,0,0,0])
            clan_players = ",".join(str(x) for x in clan_members_list_helper)
            # def a():
            #    return self.bot.wg.get_players_by_ids(clan_players,region)
            # future=self.bot.loop.run_in_executor(None,a)
            # clan_players_returned, region =await future
            clan_players_returned, region = await self.bot.wg.get_players_by_ids_a(
                clan_players, region
            )
            for clan_member, member_info in clan_players_returned.items():
                clan_member = int(clan_member)
                now = datetime.datetime.now()
                if member_info is not None:
                    self.bot.logger.debug(("process:", clan_member))
                    members_names[clan_member] = member_info["nickname"]
                    save_members_list.append(
                        [
                            clan_member,
                            member_info["statistics"]["all"]["battles"],
                            member_info["statistics"]["all"]["wins"],
                            member_info["statistics"]["all"]["damage_dealt"],
                        ]
                    )
                # clan_members_list=sorted(clan_members_list, key=itemgetter(3,5),reverse=True)
                # here we will subtract second list later

            clan_members_list = []
            for x, v in enumerate(save_members_list):
                try:
                    clan_members_list.append([v[0], v[1], (v[2] / v[1]) * 100, v[3]])
                except:
                    clan_members_list.append([v[0], v[1], 0, v[3]])

            # clan_members_list=sorted(clan_members_list, key=lambda x: (x[1],-x[2]))
            clan_members_list = sorted(clan_members_list, key=lambda x: (-x[1], x[2]))

            self.bot.logger.debug(clan_members_list)
            self.bot.logger.debug(members_names)

            with open(
                "./stats/{}-{}.pickle".format(ctx.message.author.id, clan_id), "wb"
            ) as storage:
                pickle.dump([datetime.datetime.now(), save_members_list], storage)
            # await ctx.send(out_total)
            self.bot.logger.debug(clan_members_list)
            print(humanize.naturaldelta(tstart - time.time()))
            self.bot.logger.info("end of clan stats task")


def setup(bot):
    bot.add_cog(ClanStats(bot))
