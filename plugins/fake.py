import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
import humanize
import time


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        hidden=True,
        aliases=[
            "skip",
            "play",
            "queue",
            "queu",
            "role",
            "modules",
            "?",
            "purge",
            "stats",
            "??",
            "wedgy",
            "purge30",
            "wedg",
            "clean",
            "clear",
            "volume",
            "mute",
            "unmute",
            "???",
            "????",
            "?????",
            "rps",
            "clearwarnings",
            "reason",
            "autopurge",
            "announce",
            "warn",
            "reprimand",
            "kill",
            "warnings",
            "nick",
            "ban",
            "afk",
            "stop",
            "pause",
            "say",
            "kick",
            "join",
            "playlist",
            "tag",
            "eval",
            "reminder",
            "count",
            "?join",
        ],
    )
    @asyncio.coroutine
    def command_doesn_exist(self, ctx):
        return


def setup(bot):
    bot.add_cog(Utilities(bot))
