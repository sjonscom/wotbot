import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp

percents = {
    0.0: "https://wotblitz.com/newstatic/images/twister_icon.png",
    1.0: "https://user-images.githubusercontent.com/3680926/34320161-ae8db030-e7f3-11e7-8e09-442ce9c41bde.png",
    2.0: "https://user-images.githubusercontent.com/3680926/34320162-aeb1d028-e7f3-11e7-8ff5-54e1646991a6.png",
    3.0: "https://user-images.githubusercontent.com/3680926/34320163-aeda1452-e7f3-11e7-9bc7-eae519371a5f.png",
    4.0: "https://user-images.githubusercontent.com/3680926/34320165-aef8bca4-e7f3-11e7-8f0e-c25f9554c7e8.png",
    5.0: "https://user-images.githubusercontent.com/3680926/34320166-af1bad9a-e7f3-11e7-9dc3-317b4069ddf3.png",
    6.0: "https://user-images.githubusercontent.com/3680926/34320167-af4208a0-e7f3-11e7-94f0-6f88c058596e.png",
    7.0: "https://user-images.githubusercontent.com/3680926/34320168-af623576-e7f3-11e7-93b5-b21ce29752b7.png",
    8.0: "https://user-images.githubusercontent.com/3680926/34320169-af805056-e7f3-11e7-9872-31ecfa802229.png",
    9.0: "https://user-images.githubusercontent.com/3680926/34320170-af9e6492-e7f3-11e7-944c-b1d480623d6e.png",
    10.0: "https://user-images.githubusercontent.com/3680926/34318931-d9e3abce-e7d3-11e7-9f27-ae725c0cf109.png",
}


class UserStats:
    """User statistics"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        hidden=False,
        aliases=[
            "readstatslistmany",
            "readstatslistm",
            "statslistmulti",
            "readstatslistmulti",
            "rstatslistmulti",
            "readtatslist",
        ],
    )
    # @asyncio.coroutine
    async def readstatslist(self, ctx):
        """Show stats for multiple players (names from your saved list). Save with ?savestatslist"""
        color_list = "javascript"
        my_file = Path("./stats/{}-list.pickle".format(ctx.message.author.id))
        if my_file.is_file():
            with open(
                "./stats/{}-list.pickle".format(ctx.message.author.id), "rb"
            ) as storage:
                my_list = pickle.load(storage)
                if my_list is not None:
                    save_players = my_list["default"]
        else:
            await ctx.send("No saved list")
            return

        autorun = False
        reaction = False
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier")
        all_vehicles = await self.bot.wg.get_all_vehicles()

        self.bot.logger.info("readstatslistmany")

        for player_id in save_players:
            player, region = await self.bot.wg.get_player_by_id_a(player_id)

            # def b():
            #    return self.bot.wg.get_player_by_id(player_id)
            # future1=self.bot.loop.run_in_executor(None, b)
            # player, region = await future1
            player = player[player_id]
            if player:
                autorun = False
                player_id = str(player["account_id"])
                player_nick = player["nickname"]
                self.bot.logger.info(player_nick)

                all_tiers = {}
                # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles")[player_id]
                # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
                player_tanks = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="tanks",
                    cmd_path="stats",
                    parameter="account_id={}&fields=tank_id, all.wins, all.battles,all.damage_dealt".format(
                        player_id
                    ),
                )
                player_tanks = player_tanks[player_id]
                # def a():
                #    self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
                # future = self.bot.loop.run_in_executor(None, a)
                # player_tanks = await future
                for tank in player_tanks:
                    if str(tank["tank_id"]) in all_vehicles:
                        tier = all_vehicles[str(tank["tank_id"])]["tier"]
                    else:
                        tier = 0
                    if tier not in all_tiers:
                        all_tiers[tier] = tank["all"]
                    else:
                        all_tiers[tier] = dict(
                            Counter(all_tiers[tier]) + Counter(tank["all"])
                        )

                player_rating, player_rating_league_icon, player_rating_league = await self.bot.wg.get_rating(
                    player_id, region, ctx, score=1
                )
                self.bot.logger.debug(all_tiers)
                wr_ttl = None
                old_tiers = False
                my_file = Path(
                    "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id)
                )
                # self.bot.logger.debug(my_file)
                if my_file.is_file():
                    with open(
                        "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id),
                        "rb",
                    ) as storage:
                        old_tiers = pickle.load(storage)
                else:
                    my_file = Path("./stats/{}.pickle".format(player_id))
                    if my_file.is_file():
                        with open(
                            "./stats/{}.pickle".format(player_id), "rb"
                        ) as storage:
                            old_tiers = pickle.load(storage)
                if old_tiers:
                    ago = datetime.datetime.now() - old_tiers[0]
                    ago = humanize.naturaltime(ago)
                    ago = "Reading data for `{}@{}`, saved {}.".format(
                        player_nick, region, ago
                    )
                    # self.bot.logger.debug(old_tiers[1])
                    player_rating_old = None
                    if len(old_tiers) > 2:
                        player_rating_old = old_tiers[2].get("rating", 0)
                    damage_total = True

                    if player_rating_old is not None and player_rating is not None:
                        if isinstance(player_rating_old, str):
                            player_rating = "{}, try re-save".format(player_rating_old)
                        elif isinstance(player_rating, str):
                            player_rating = "{}, try re-save".format(player_rating)
                        else:
                            player_rating = player_rating - player_rating_old
                    else:
                        player_rating = "unsaved yet"
                    for j in old_tiers[1]:
                        if "damage_dealt" not in old_tiers[1][j]:
                            old_tiers[1][j]["damage_dealt"] = all_tiers[j][
                                "damage_dealt"
                            ]
                            damage_total = False

                    self.bot.logger.debug(old_tiers)

                    result_tier = []
                    result_total = Counter()
                    for i in all_tiers:
                        # self.bot.logger.debug((Counter(all_tiers[i]), Counter(old_tiers[1].get(i,{'battles': 0, 'wins': 0,'damage_dealt':0}))))
                        temp = Counter(all_tiers[i]) - Counter(
                            old_tiers[1].get(
                                i, {"battles": 0, "wins": 0, "damage_dealt": 0}
                            )
                        )
                        if temp["battles"] > 0:
                            # temp=temp+Counter({'battles': 0, 'wins': 0,'damage_dealt':0})
                            temp = Counter(all_tiers[i])
                            temp.subtract(
                                Counter(
                                    old_tiers[1].get(
                                        i, {"battles": 0, "wins": 0, "damage_dealt": 0}
                                    )
                                )
                            )

                        self.bot.logger.debug(temp)

                        result_total.update(temp)
                        if temp:
                            # result_tier.append([i,temp["battles"],temp["wins"]/temp["battles"]*100,temp["damage_dealt"]/temp["battles"]])
                            try:
                                result_tier.append(
                                    [
                                        i,
                                        temp["battles"],
                                        temp["wins"] / temp["battles"] * 100,
                                        temp["damage_dealt"] / temp["battles"],
                                    ]
                                )
                            except:
                                result_tier.append([i, temp["battles"], "0", "0"])
                    self.bot.logger.debug(result_total)

                    if len(result_tier) > 0:
                        if damage_total:
                            foot = _(
                                "Create new checkpoint at anytime with ?savestats . Due to changes in update 5.5, some tiers might show strange results, ?savestats is recommended."
                            )
                        else:
                            foot = _(
                                "Some tanks have unsaved DMG, calculated damages are not accurate. Re-save is recommended. Create new checkpoint at anytime with ?savestats"
                            )
                        result_tier = sorted(result_tier, key=lambda x: (x[0], x[3]))
                        result_tier.append(
                            [
                                _("Total:"),
                                result_total["battles"],
                                result_total["wins"] / result_total["battles"] * 100,
                                result_total["damage_dealt"] / result_total["battles"],
                            ]
                        )
                        wr_ttl = result_total["wins"] / result_total["battles"] * 100
                        toutotal = "{}\n{} {}\n".format(
                            tabulate(
                                result_tier,
                                headers=[_("Tier"), _("Battles"), _("WR"), _("DMG")],
                                floatfmt=".2f",
                                stralign="right",
                                numalign="right",
                            ),
                            _("Rating difference:"),
                            player_rating,
                        )
                    else:
                        toutotal = "{}\n{} {}\n".format(
                            _("No battles since checkpoint."),
                            _("Rating difference:"),
                            player_rating,
                        )
                        foot = _("No data right now.")

                else:
                    ago = _("No data for `{}@{}`. First make a checkpoint:").format(
                        player_nick, region
                    )
                    toutotal = "?savestatslist"
                    foot = _("Let me try to do it for you right now...")
                    autorun = True
                    # self.bot.dispatch('command',command,ctx)
                    # await command.invoke(ctx)

                embed = discord.Embed(
                    title=ago,
                    description="```{}\n{}```".format(color_list, toutotal),
                    colour=234,
                    type="rich",
                )
                # embed.add_field(name='\uFEFF', value="```{}```".format(o))
                # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
                embed.set_footer(text="{}".format(foot))
                if wr_ttl is not None:
                    embed.set_thumbnail(url=percents[round(wr_ttl, -1) / 10])
                try:
                    await ctx.send(content=None, embed=embed)
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.send(
                        content=_("Please enable Embed links permission for wotbot.")
                    )
                    break
                # if reaction:
                # await self.bot.add_reaction(msg,'\N{THUMBS UP SIGN}') #test of votable emoji → "reaction"
                # await self.bot.add_reaction(msg,'\N{THUMBS DOWN SIGN}') #test of votable emoji → "reaction"
                if autorun:
                    command = self.bot.all_commands["savestats"]
                    await ctx.invoke(command, player_name=player_nick)
            else:
                out = await self.bot.wg.search_player_a(ctx, player_name)
                await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
