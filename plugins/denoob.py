import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
from collections import Counter
import aiohttp


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["denub"])
    # @asyncio.coroutine
    async def denoob(self, ctx, player_name: str = None, plus_percent: str = None):
        """Denoob your stats one tier at a time."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)
        color_total = "json"

        tier_to_image = {
            "1": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/RenaultFT.png",
            "2": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/GB05_Vickers_Medium_Mk_II.png",
            "3": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/G100_Gtraktor_Krupp.png",
            "4": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/T-28.png",
            "5": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/SU-85.png",
            "6": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/GB19_Sherman_Firefly.png",
            "7": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/AMX_dracula.png",
            "8": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/Ch01_Type59.png",
            "9": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/T30.png",
            "10": "http://glossary-eu-static.gcdn.co/icons/wotb/current/uploaded/vehicles/hd_thumbnail/T110E4.png",
        }

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("denoob:", player_name, plus_percent))
        if plus_percent is None:
            plus_percent = 5
        else:
            try:
                plus_percent = int(plus_percent)
            except:
                await ctx.send(
                    _(
                        "use: denoob or denoob [name] or denoob [player_name] [plus_percent]"
                    )
                )
                return

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier, images.preview")

        all_vehicles = await self.bot.wg.get_all_vehicles()

        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            url = "https://www.blitzstars.com/api/top/player/{}".format(player_id)
            async with aiohttp.ClientSession(loop=self.bot.loop) as client:
                async with client.get(url) as r:
                    data = None
                    if r.status == 200:
                        data = await r.json(content_type=None)
                        if (data) and (len(data) > 0):
                            self.bot.logger.debug(data)
                        else:
                            await ctx.send(_("No data on BlitzStars"))
                            return
                    # print(data, player_id,r.headers)

            all_tiers = {}
            # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id,all.wins,all.battles,all.damage_dealt".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]

            if player_tanks is None:
                await ctx.send(_("No data for `{}@{}` ").format(player_nick, region))
                return
            for tank in player_tanks:
                if str(tank["tank_id"]) in all_vehicles:
                    tier = all_vehicles[str(tank["tank_id"])]["tier"]
                else:
                    tier = 0
                if tier not in all_tiers:
                    all_tiers[tier] = tank["all"]
                else:
                    all_tiers[tier] = dict(
                        Counter(all_tiers[tier]) + Counter(tank["all"])
                    )
            self.bot.logger.debug(all_tiers)

            if data is None:
                await ctx.send(_("No data on BlitzStars"))
                return
            else:

                # print("90daysdata",data["period90d"])
                if data["period90d"] is None:
                    toutotal = _("No data on BlitzStars.")
                    await ctx.send(_("No data on BlitzStars"))
                    return
                else:
                    if not data["period90d"].get("all", False):
                        await ctx.send(_("No data on BlitzStars"))
                        return
                    # data90=data["period90d"]["tierData"]["tiers"]
                    data90 = data["period90d"]["special"]

                if data90.get("winrate", None) is None:
                    await ctx.send(_("No data on BlitzStars"))
                    return
                title_text = _("Denoobing")
                embed = discord.Embed(
                    title="{title} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                        title=title_text,
                        for_=_("for"),
                        nick=player_nick,
                        region=region,
                        confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                        contributor=await self.bot.dc.contributor(ctx, player, region),
                    ),
                    colour=234,
                    type="rich",
                    url=await self.bot.dc.confirmed_url(ctx, ownuser),
                    # url="https://www.blitzstars.com/player/{}/{}".format(
                    #    region, player_nick
                    # ),
                )
                for tx in range(1, 11):
                    _tier = str(tx)

                    if all_tiers.get(int(_tier), None) is None:
                        embed.add_field(
                            name=_("Tier {}").format(_tier), value=_("No data")
                        )
                    else:
                        try:

                            # self.bot.logger.debug(data90)
                            battles = all_tiers[int(_tier)]["battles"]
                            wins = all_tiers[int(_tier)]["wins"]
                            wr = None
                            wr = wins / battles * 100
                            # self.bot.logger.info((all_tiers[int(_tier)], wr, _tier))

                            # battles90=data90[_tier]["battles"]
                            # wins90=data90[_tier]["wins"]
                            # wr90=wins90/battles90*100
                            wr90 = data90["winrate"]
                            # self.bot.logger.debug((battles, wins, wr))

                            shortgoal = math.ceil(wr)
                            # self.bot.logger.debug(shortgoal)
                            midgoal = shortgoal + plus_percent
                            # self.bot.logger.debug(midgoal)
                            shortbattles90 = math.ceil(
                                (battles * (shortgoal - wr)) / (wr90 - shortgoal)
                            )
                            shortbattles100 = math.ceil(
                                (battles * (shortgoal - wr)) / (100 - shortgoal)
                            )
                            midbattles90 = math.ceil(
                                (battles * (midgoal - wr)) / (wr90 - midgoal)
                            )
                            midbattles100 = math.ceil(
                                (battles * (midgoal - wr)) / (100 - midgoal)
                            )

                            battles_90 = data["period90d"]["all"]["battles"]
                            bpd_90 = battles_90 / 90

                            # ((Total Battles Now x Target/Goal WR) - (Total # of Wins Right Now * 100)) / (Recent WR - Target/Goal WR)
                            # var battles_needed_for_WR = Math.ceil((battle_count * (target_WR - old_WR ) ) / (current_WR - target_WR ));
                            toutotal = _(
                                "To get to {} wr (+{:.2f}%), you {} with current 90day wr of {:.2f} {} can play {} battles with 100% winrate.\n\n"
                            ).format(
                                shortgoal,
                                shortgoal - wr,
                                _(
                                    "need to play {} battles (this would take you about {} days with your average of {} battles per day),"
                                ).format(
                                    shortbattles90,
                                    math.ceil(shortbattles90 / bpd_90),
                                    math.ceil(bpd_90),
                                )
                                if shortbattles90 > 0
                                else _("cannot reach"),
                                wr90,
                                _("or") if shortbattles90 > 0 else _("but"),
                                shortbattles100,
                            )
                            toutotal += _(
                                "To get to {} wr (+{:.2f}%), you {} with current 90day wr of {:.2f} {} can play {} battles with 100% winrate."
                            ).format(
                                midgoal,
                                midgoal - wr,
                                _("need to play {} battles").format(midbattles90)
                                if midbattles90 > 0
                                else _("cannot reach"),
                                wr90,
                                _("or") if midbattles90 > 0 else _("but"),
                                midbattles100,
                            )
                        except ArithmeticError as e:
                            if wr is not None:
                                if wr == 100:
                                    toutotal = _("Well done, 100% winrate!")
                            else:
                                toutotal = _(
                                    "Cannot calculate due to ArithmeticError: {}. One of goals is the same as one of winrates. Try again in near future, when your stats change a bit."
                                ).format(e)
                            # toutotal=" Cannot calculate due to ArithmeticError: {}. One of goals ({}, {}) is the same as one of winrates ({}, 100). Try again in near future, when your stats change a bit.".format(e,shortgoal, midgoal, wr90))
                        embed.add_field(
                            name=_("Tier {}, wr: {:.2f}%").format(_tier, wr),
                            value="```{color}\n{out}```".format(
                                color=color_total, out=toutotal
                            ),
                        )

            embed.set_thumbnail(url="{}".format(tier_to_image[_tier]))
            embed.set_footer(
                text=_("90day wr data by BlitzStars."),
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    _("Please enable Embed links permission for wotbot.")
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
