import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math
from random import randint, uniform, shuffle

emojis = ["\U0001f1e6", "\U0001f1e7", "\U0001f1e8"]
# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf


class BlitzQuiz:
    """Quiz and knowledge helper"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True, hidden=False, aliases=["triviatop", "blitzquiztop"]
    )
    # @asyncio.coroutine
    async def quiztop(self, ctx):
        """BlitzQuiz. Top players"""
        self.bot.logger.info("quiztop:")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        res = await self.bot.sqlite.get_quiz_top()
        # print(res)
        out = []
        for i in res:
            user = await self.bot.get_user_info(i[0])
            out.append((user.name, i[1], i[2], round(int(i[2]) / int(i[1]) * 100, 2)))
        out.sort(key=lambda tup: tup[3], reverse=True)
        if res:
            try:
                out = tabulate(
                    out,
                    headers=["discord name", "questions", "correct", "winrate"],
                    tablefmt="grid",
                )
                await ctx.send(_("Top 5 BlitzQuiz players:\n\n`{}`").format(out))
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.message.author.send(
                    content=_(
                        "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                    )
                )
        else:
            try:
                await ctx.send(_("No score yet"))
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.message.author.send(
                    content=_(
                        "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                    )
                )

    @commands.command(
        pass_context=True, hidden=False, aliases=["triviastats", "blitzquizstats"]
    )
    # @asyncio.coroutine
    async def quizstats(self, ctx):
        """BlitzQuiz. Your stats."""
        self.bot.logger.info("quiz:")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if ctx.message.mentions:
            for user in ctx.message.mentions:
                print(user.id)
                res = await self.bot.sqlite.get_quiz(user.id)
                if res:
                    print(res)
                    try:
                        await ctx.send(
                            _(
                                "BlitzQuiz score for {}\n\nNumber of quizes: {}\nCorrect answers: {}\nWinrate: {:4.2f}"
                            ).format(
                                user.name,
                                res[1],
                                res[2],
                                int(res[2]) / int(res[1]) * 100,
                            )
                        )
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.message.author.send(
                            content=_(
                                "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                            )
                        )
                else:
                    try:
                        await ctx.send(_("No score for {}").format(user.name))
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.message.author.send(
                            content=_(
                                "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                            )
                        )
        else:
            res = await self.bot.sqlite.get_quiz(ctx.message.author.id)
            print(res)
            if res:
                try:
                    await ctx.send(
                        _(
                            "Your BlitzQuiz score\n\nNumber of quizes: {}\nCorrect answers: {}\nWinrate: {:4.2f}"
                        ).format(res[1], res[2], int(res[2]) / int(res[1]) * 100)
                    )
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.message.author.send(
                        content=_(
                            "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                        )
                    )
            else:
                try:
                    await ctx.send(_("No score yet"))
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.message.author.send(
                        content=_(
                            "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                        )
                    )

    @commands.command(
        pass_context=True, hidden=False, aliases=["trivia", "blitzquiz", "guiz"]
    )
    # @asyncio.coroutine
    async def quiz(self, ctx):
        """BlitzQuiz. See how you stack up. Play!"""
        self.bot.logger.info("quiz:")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        out = self.bot.gamedata.quiz()

        ammo_names = {
            "HOLLOW_CHARGE": _("HEAT"),
            "ARMOR_PIERCING": _("AP"),
            "ARMOR_PIERCING_PREMIUM": _("AP Premium"),
            "HIGH_EXPLOSIVE": _("HE"),
            "HIGH_EXPLOSIVE_PREMIUM": _("HE Premium"),
            "ARMOR_PIERCING_CR": _("APCR"),
        }
        if out is None:
            await ctx.send(_("Could find anything"))
            return

        topics = ["damage", "penetration", "reload", "elevation", "depression"]
        topics_names = {
            "damage": _("damage"),
            "penetration": _("penetration"),
            "reload": _("reload"),
            "elevation": _("elevation"),
            "depression": _("depression"),
        }
        topic = topics[randint(0, len(topics) - 1)]
        # print("GUNY", out["guns"])
        # print(type(out["guns"]))
        gun = list(out["guns"].items())[randint(0, len(out["guns"]) - 1)]
        # print(gun)
        ammo_type_desc = ""
        answers = []
        ammo_type = list(gun[1]["ammo"].items())[randint(0, len(gun[1]["ammo"]) - 1)]
        if topic in ["damage", "penetration"]:  # damage, pene
            ammo_type_name = ammo_type[0].replace("_", " ")
            ammo_type_shell = ammo_names[ammo_type[1]["name"]]
            ammo_type_desc = _("with **{}** shell {} ").format(
                ammo_type_shell, ammo_type_name
            )

        if topic == "damage":
            # print("damage:", ammo_type[1]["damage"])
            gun_damage = round(float(ammo_type[1]["damage"]), 1)
            answer = str(gun_damage)
            answers.append(str(gun_damage))
            answers.append(str(round(gun_damage * uniform(0.2, 2), 1)))
            answers.append(str(round(gun_damage * uniform(0.2, 2), 1)))
        elif topic == "reload":
            # print("reload:", gun[1]["reload"])
            gun_reload = round(float(gun[1]["reload"]), 1)
            answers.append(str(gun_reload))
            answers.append(str(round(gun_reload * uniform(0.2, 2), 1)))
            answers.append(str(round(gun_reload * uniform(0.2, 2), 1)))
            answer = str(gun_reload)
        elif topic == "penetration":
            # print("penetration", ammo_type[1]["penetration"])
            gun_penetration = round(float(ammo_type[1]["penetration"]), 1)
            answers.append(str(gun_penetration))
            answers.append(str(round(gun_penetration * uniform(0.2, 2), 1)))
            answers.append(str(round(gun_penetration * uniform(0.2, 2), 1)))
            answer = str(gun_penetration)
        elif topic == "elevation":
            # print("elevation", gun[1]["pitchLimits"])
            gun_elevation = round(float(gun[1]["pitchLimits"].split()[0]) * -1, 0)
            answers.append(str(gun_elevation))
            answers.append(str(round(gun_elevation * uniform(0.2, 2), 0)))
            answers.append(str(round(gun_elevation * uniform(0.2, 2), 0)))
            answer = str(gun_elevation)
        elif topic == "depression":
            # print("depression", gun[1]["pitchLimits"])
            gun_depression = round(float(gun[1]["pitchLimits"].split()[1]), 0)
            answers.append(str(gun_depression))
            answers.append(str(round(gun_depression * uniform(0.2, 2), 0)))
            answers.append(str(round(gun_depression * uniform(0.2, 2), 0)))
            answer = str(gun_depression)
        embed = discord.Embed(
            title=_("BlitzQuiz Question:"),
            description=_(
                "The tier {gun_tier} gun **{gun_name}** on **{tank_name}** (tier {tank_tier}) {ammo}has defined **{topic}**:"
            ).format(
                gun_tier=gun[1]["tier"],
                gun_name=gun[0].replace("_", " "),
                tank_name=out["name"],
                tank_tier=out["tier"],
                ammo=ammo_type_desc,
                topic=topics_names[topic],
            ),
            colour=234,
            type="rich",
        )
        embed.set_footer(
            text="{}".format(_("BlitzQuiz delivered by \U0001f451 WotBot"))
        )

        shuffle(answers)
        answer_emoji = emojis[answers.index(answer)]

        embed.add_field(
            inline=False,
            name="\uFEFF",
            value=":regional_indicator_a: {}".format(answers[0]),
        )
        embed.add_field(
            inline=False,
            name="\uFEFF",
            value=":regional_indicator_b: {}".format(answers[1]),
        )
        embed.add_field(
            inline=False,
            name="\uFEFF",
            value=":regional_indicator_c: {}".format(answers[2]),
        )

        # embed.set_thumbnail(url=percents[round(wr_ttl, -1)/10])
        try:
            msg = await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )
            return

        try:
            await msg.add_reaction("\U0001f1e6")  # test of votable emoji → "reaction"
            await asyncio.sleep(0.5)
            await msg.add_reaction("\U0001f1e7")  # test of votable emoji → "reaction"
            await msg.add_reaction("\U0001f1e8")  # test of votable emoji → "reaction"
        except discord.Forbidden:
            self.bot.logger.warning(
                "Please enable Add reactions permission for wotbot."
            )
            await ctx.send(
                content=_("Please enable Add reactions permission for WotBot.")
            )

        await asyncio.sleep(0.1)
        # res=await self.bot.wait_for_reaction(['\U0001f1e6','\U0001f1e7','\U0001f1e8'], message=msg,user=ctx.message.author)
        def check(reaction, user):
            if msg.id == reaction.message.id:
                if user != msg.author:
                    return reaction, user

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", check=check, timeout=60
            )
            if reaction.emoji == answer_emoji:
                msg2 = await ctx.send(
                    _("Correct! Press :arrow_forward: for next question.")
                )
                await self.bot.sqlite.add_quiz(user.id, 1)
            else:
                msg2 = await ctx.send(
                    _(
                        "Wrong, answer was {} , press :arrow_forward: for next question."
                    ).format(answer)
                )
                await self.bot.sqlite.add_quiz(user.id, 0)
        except asyncio.TimeoutError:
            msg2 = await ctx.send(
                _("Too late, try again! Press :arrow_forward: for next question.")
            )
            print("timed out")

        print("remove", msg.reactions)
        for emoji in emojis:
            try:
                await msg.remove_reaction(emoji, msg.author)
            except Exception as e:
                self.bot.logger.info(e)
            await asyncio.sleep(0.3)

        try:
            await msg2.add_reaction("\U000025b6")
        except:
            pass

        await asyncio.sleep(0.1)

        def check2(reaction, user):
            if msg2.id == reaction.message.id:
                if user != msg2.author:
                    return reaction, user

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", check=check2, timeout=300
            )
            if reaction.emoji:
                command = self.bot.all_commands["quiz"]
                await ctx.invoke(command)
        except asyncio.TimeoutError:
            print("timed out")
        try:
            await msg2.remove_reaction("\U000025b6", msg.author)
        except Exception as e:
            self.bot.logger.info(e)

    @commands.command(pass_context=True, hidden=False, aliases=["oracle", "whisperer"])
    # @asyncio.coroutine
    async def whisper(self, ctx, *, tank: str = None):
        """Oracle whisperer. No cheating!"""
        # self.bot.logger.info(("whisper", tank))
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if tank is None:
            await ctx.send(_("must provide tank name"))
            return
        out = self.bot.gamedata.hledej(tank)
        print(out)

        ammo_names = {
            "HOLLOW_CHARGE": _("HEAT"),
            "ARMOR_PIERCING": _("AP"),
            "ARMOR_PIERCING_PREMIUM": _("AP Premium"),
            "HIGH_EXPLOSIVE": _("HE"),
            "HIGH_EXPLOSIVE_PREMIUM": _("HE Premium"),
            "ARMOR_PIERCING_CR": _("APCR"),
        }
        if out is None:
            await ctx.send("Could find anything")
            return

        embed = discord.Embed(
            title="{}, tier: {}".format(out["name"], out["tier"]),
            description=_("Guns"),
            colour=234,
            type="rich",
        )
        embed.set_footer(text="{}".format(out["name"]))
        for gun, items in out["guns"].items():
            ammo1 = []
            ammo2 = []
            ammo3 = []
            for ammo, data in items["ammo"].items():
                ammo1.append(data["damage"])
                ammo2.append(data["penetration"])
                ammo3.append(ammo_names[data["name"]])

            ammo_final = _("Ammo: {}\nPenetration: {}mm\nDamage: {}HP").format(
                "/".join(ammo3), "/".join(ammo2), "/".join(ammo1)
            )

            embed.add_field(
                inline=False,
                name=gun.replace("_", " "),
                value=_(
                    "```Tier: {}\nReload: {}sec.\n{}\nDepression: {}°\nElevation: {}°```"
                ).format(
                    items["tier"],
                    round(float(items["reload"]), 2),
                    ammo_final,
                    items["pitchLimits"].split()[1],
                    float(items["pitchLimits"].split()[0]) * -1,
                ),
            )

        # embed.set_thumbnail(url=percents[round(wr_ttl, -1)/10])
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )


def setup(bot):
    bot.add_cog(BlitzQuiz(bot))
