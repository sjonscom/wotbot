import discord
from discord.ext import commands
import datetime
import re
import aiohttp
from glom import glom
from datetime import datetime
import json


class News:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=True)
    async def wgnews(self, ctx):
        """Check news"""
        print("check news")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        lang = "en"
        size = "3"
        baseurl = "https://wotblitz.ru"

        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            url = "https://wotblitz.ru/{lang}/api/cms/news/?category=&page=1&page_size={size}".format(
                lang=lang, size=size
            )
            async with client.get(url) as r:
                data = None
                if r.status == 200:
                    data = await r.json(content_type=None)
                    if data is None:
                        return
                        # data=json.loads(r.text)
                        self.bot.logger.debug(data)

        # lambda expression to process data further...
        # from glom import T
        # a = glom(
        #    b,
        #    (
        #        "results",
        #        [
        #            {
        #                "type": (T, lambda a: "{}/{}".format(a["slug"], "...")),
        #                "title": "translation.title",
        #                "description": "translation.description",
        #                "lang": "lang",
        #                "category": "category.slug",
        #                "slug": "slug",
        #                "image": "images.client",
        #                "date": (
        #                    "publication_start_at",
        #                    lambda t: datetime.strptime(t, "%Y-%m-%dT%H:%M:%S"),
        #                ),
        #            }
        #        ],
        #    ),
        # )

        articles = glom(
            data,
            (
                "results",
                [
                    {
                        "title": "translation.title",
                        "description": "translation.description",
                        "lang": "lang",
                        "category": "category.slug",
                        "slug": "slug",
                        "image": "images.client",
                        "thumbnail": "images.small",
                        "date": (
                            "publication_start_at",
                            lambda t: datetime.strptime(t, "%Y-%m-%dT%H:%M:%S"),
                        ),
                    }
                ],
            ),
        )
        if len(articles):
            article = articles[0]
            # print(article)

            embed = discord.Embed(
                title=article.get("title"),
                description=article.get("description"),
                timestamp=article.get("date"),
                colour=234,
                type="rich",
                url="{url}/{lang}/news/{category}/{slug}".format(
                    url=baseurl, **article
                ),
            )

            embed.set_author(
                name="Wargaming.net",
                url="{url}/lang/news/".format(url=baseurl),
                icon_url="https://cdn.discordapp.com/attachments/370945953550696449/531585390797389845/world-of-tanks-blitz-metallic-logo.png",
            )

            embed.set_image(url=article.get("image"))
            embed.set_footer(text=_("Delivered by \U0001f451 WotBot."))

            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )


def setup(bot):
    bot.add_cog(News(bot))
