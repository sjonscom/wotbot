import discord
from discord.ext import commands
import asyncio
import datetime
import aiohttp


class Utilities:
    """Server population and other tools"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        parent="Utilities",
        pass_context=True,
        # brief="brief help",
        # description="desc help",
        # help="help help text",
    )
    # @asyncio.coroutine
    async def blitztype(self, ctx):
        """WOT Blitz BlitzTypeKeyboard for Android."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        embed = discord.Embed(
            title="BlitzTypeKeyboard",
            description=_(
                "BlitzTypeKeyboard is a WOT Blitz dedicated keyboard for Android devices.\n[Apk download]({})\n[Description and source code]({})"
            ).format(
                "https://github.com/vanous/BlitzTypeKeyboard/releases/download/V1.0.4-beta/BlitzTypeKeyboard_1.0.4-beta.apk",
                "https://github.com/vanous/BlitzTypeKeyboard",
            ),
            colour=234,
            type="rich",
            url="https://github.com/vanous/BlitzTypeKeyboard/releases/download/V1.0.4-beta/BlitzTypeKeyboard_1.0.4-beta.apk",
        )
        embed.set_author(
            name="b48g55m",
            url="https://github.com/vanous/",
            icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png",
        )
        embed.set_image(
            url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/Screenshot_2017-01-01-00-24-51.png"
        )
        embed.set_footer(
            text=_("Designed for World of Tanks Blitz"),
            icon_url="https://wotblitz.eu/dcont/1.5.0.1/fb/image/rsz_blitz_logo.png",
        )
        # embed.set_image(url="https://cloud.githubusercontent.com/assets/3680926/21720861/c8d78a8e-d425-11e6-80a9-39ff85c5e30b.png")
        embed.set_thumbnail(
            url="https://raw.githubusercontent.com/vanous/BlitzTypeKeyboard/master/bt_logo_big.png"
        )
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )


def setup(bot):
    bot.add_cog(Utilities(bot))
