import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["master", "masteries"])
    # @asyncio.coroutine
    async def mastery(self, ctx, player_name: str = None):
        self.bot.logger.info(ctx.message.author)

        """Show list of tanks with ACE Mark Of Mastery"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        autorun = False
        mastery_marks = {
            4: _("Ace Tanker"),
            3: _("1st Class"),
            2: _("2nd Class"),
            1: _("3rd Class"),
            0: _("No mastery"),
        }

        all_vehicles = await self.bot.wg.get_all_vehicles()

        self.bot.logger.info(("mastery:", player_name))
        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return

        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tanks = []

            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id,mark_of_mastery".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]

            player_tanks_achievements = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="achievements",
                parameter="account_id={}&fields=tank_id,achievements".format(player_id),
            )
            player_tanks_achievements = player_tanks_achievements[player_id]

            if not player_tanks_achievements:
                await ctx.send(
                    _("No data from WG for `{}@{}` ").format(player_nick, region)
                )
                return
            player_tanks_achievements = {
                x["tank_id"]: x["achievements"] for x in player_tanks_achievements
            }

            if player_tanks:
                for tank in player_tanks:
                    markOfMastery = tank.get("mark_of_mastery", 0)
                    if markOfMastery == 4:
                        if (
                            player_tanks_achievements[tank["tank_id"]].get(
                                "markOfMastery", 0
                            )
                            != 4
                        ):
                            markOfMasteryCount = player_tanks_achievements[
                                tank["tank_id"]
                            ].get("markOfMastery", 0)
                        else:
                            # best effort detection
                            markOfMasteryI = player_tanks_achievements[
                                tank["tank_id"]
                            ].get("markOfMasteryI", None)
                            markOfMasteryII = player_tanks_achievements[
                                tank["tank_id"]
                            ].get("markOfMasteryII", None)
                            markOfMasteryIII = player_tanks_achievements[
                                tank["tank_id"]
                            ].get("markOfMasteryIII", None)
                            if (
                                markOfMasteryI is None
                                and markOfMasteryII is None
                                and markOfMasteryIII is None
                            ):
                                markOfMasteryCount = 1  # best effort
                            else:
                                markOfMasteryCount = player_tanks_achievements[
                                    tank["tank_id"]
                                ].get("markOfMastery", 0)

                        if str(tank["tank_id"]) in all_vehicles:
                            tank_name = all_vehicles[str(tank["tank_id"])]["name"]
                            tank_tier = all_vehicles[str(tank["tank_id"])]["tier"]
                        else:
                            print("tank missing in tankopedia:", tank)
                            tank_name = _("Not in tankopedia")
                            tank_tier = 0
                        if markOfMasteryCount == 1:
                            markOfMasteryCount = ""
                        all_tanks.append([tank_tier, tank_name, markOfMasteryCount])

                all_tanks_sorted = sorted(all_tanks, key=itemgetter(0), reverse=True)
                self.bot.logger.debug(len(all_tanks_sorted))
                for u in range(0, len(all_tanks_sorted), 70):
                    toutotal = "{}\n".format(
                        tabulate(
                            all_tanks_sorted[u : u + 70],
                            floatfmt=".2f",
                            stralign="left",
                            numalign="right",
                        )
                    )
                    self.bot.logger.debug(len(str(toutotal)))
                    title_text = _("Mastery list")
                    embed = discord.Embed(
                        title="""{title} {for_} `{nick}@{region}` {for_} {mark}\n{confirmed}\n{contributor}""".format(
                            title=title_text,
                            for_=_("for"),
                            nick=player_nick,
                            region=region,
                            mark=mastery_marks[4],
                            confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                            contributor=await self.bot.dc.contributor(
                                ctx, player, region
                            ),
                        ),
                        description=_("```Tier/Tank/Count >1\n{}```").format(toutotal),
                        url=await self.bot.dc.confirmed_url(ctx, ownuser),
                        colour=234,
                        type="rich",
                    )
                    embed.set_thumbnail(
                        url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery{}.png".format(
                            4
                        )
                    )
                    embed.set_footer(
                        text=_(
                            "Good job, {} tanks with {}.\n Tier 0 are tanks not in WG tankopedia. Occasionally wrong tank ACE count by Wargaming."
                        ).format(len(all_tanks), mastery_marks[4]),
                        icon_url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(
                            4
                        ),
                    )
                    try:
                        await ctx.send(content=None, embed=embed)
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.send(
                            content=_(
                                "Please enable Embed links permission for wotbot."
                            )
                        )

                msg = await ctx.send(
                    content=_("For no ACE mastery list, click the {} button").format(
                        "\U0001f464"
                    )
                )
                try:
                    await msg.add_reaction(
                        "\U0001f464"
                    )  # test of votable emoji → "reaction"
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Add reactions permission for wotbot."
                    )
                    await ctx.send(
                        content=_("Please enable Add reactions permission for WotBot.")
                    )

                await asyncio.sleep(0.1)
                # res=await self.bot.wait_for_reaction(['\U0001f1e6','\U0001f1e7','\U0001f1e8'], message=msg,user=ctx.message.author)
                def check(reaction, user):
                    if msg.id == reaction.message.id:
                        if user != msg.author:
                            return reaction, user

                try:
                    reaction, user = await self.bot.wait_for(
                        "reaction_add", check=check, timeout=60
                    )
                    if reaction.emoji == "\U0001f464":
                        command = self.bot.all_commands["nomastery"]
                        await ctx.invoke(command, player_name=player_nick)

                except asyncio.TimeoutError:
                    print("timed out")

                print("remove", msg.reactions)
                try:
                    await msg.remove_reaction("\U0001f464", msg.author)
                except Exception as e:
                    self.bot.logger.info(e)

                try:
                    await msg.add_reaction("\U00002611")
                except:
                    pass

            else:
                await ctx.send(_("No data."))
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)

    @commands.command(pass_context=True, hidden=True)
    async def nomastery(self, ctx, player_name: str = None):
        self.bot.logger.info(ctx.message.author)
        """Show list of tanks with ACE Mark Of Mastery"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        autorun = False
        mastery_marks = {
            4: _("Ace Tanker"),
            3: _("1st Class"),
            2: _("2nd Class"),
            1: _("3rd Class"),
            0: _("No mastery"),
        }

        all_vehicles = await self.bot.wg.get_all_vehicles()

        self.bot.logger.info(("mastery:", player_name))
        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        self.bot.logger.info(("own:", ownuser))
        if not player:
            return

        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tanks = []

            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id,mark_of_mastery".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]

            player_tanks_achievements = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="achievements",
                parameter="account_id={}&fields=tank_id,achievements".format(player_id),
            )
            player_tanks_achievements = player_tanks_achievements[player_id]
            player_tanks_achievements = {
                x["tank_id"]: x["achievements"] for x in player_tanks_achievements
            }

            if player_tanks:
                for tank in player_tanks:
                    markOfMastery = tank.get("mark_of_mastery", 0)
                    if markOfMastery != 4:

                        if str(tank["tank_id"]) in all_vehicles:
                            tank_name = all_vehicles[str(tank["tank_id"])]["name"]
                            tank_tier = all_vehicles[str(tank["tank_id"])]["tier"]
                        else:
                            print("tank missing in tankopedia:", tank)
                            tank_name = _("Not in tankopedia")
                            tank_tier = 0
                        all_tanks.append([tank_tier, tank_name])

                all_tanks_sorted = sorted(all_tanks, key=itemgetter(0), reverse=True)
                self.bot.logger.debug(len(all_tanks_sorted))
                for u in range(0, len(all_tanks_sorted), 70):
                    toutotal = "{}\n".format(
                        tabulate(
                            all_tanks_sorted[u : u + 70],
                            floatfmt=".2f",
                            stralign="left",
                            numalign="right",
                        )
                    )
                    self.bot.logger.debug(len(str(toutotal)))
                    title_text = _("No mastery list")
                    embed = discord.Embed(
                        title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                            title=title_text,
                            for_=_("for"),
                            nick=player_nick,
                            region=region,
                            confirmed="",  # await self.bot.dc.confirmed(ctx, ownuser),
                            contributor=await self.bot.dc.contributor(
                                ctx, player, region
                            ),
                        ),
                        description=_("```Tier/Tank\n{}```").format(toutotal),
                        # url=await self.bot.dc.confirmed_url(ctx, ownuser),
                        colour=234,
                        type="rich",
                    )
                    embed.set_thumbnail(
                        url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery{}.png".format(
                            4
                        )
                    )
                    embed.set_footer(
                        text=_(
                            "{} tanks without {}.\n Tier 0 are tanks not in WG tankopedia."
                        ).format(len(all_tanks), mastery_marks[4]),
                        icon_url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(
                            4
                        ),
                    )
                    try:
                        await ctx.send(content=None, embed=embed)
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.send(
                            content=_(
                                "Please enable Embed links permission for wotbot."
                            )
                        )

        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
